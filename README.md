# Shuffle
## Shawn Rodgers

### Description
Shuffle is a Spotify web interface that allows you to view your playlists, and all of the songs in them. It collects data using the Spotify Web API. Currently the only functionality is to view your library, but I plan to add the ability to add to it as well soon. I also Plan to add more analytical features to the app to let users see data about their music tastes. Spotify provides lots of intersting data points for songs and playlists so I have plenty to experiment with. 

### Setup
Currently this project is not setup for other users to test, but it will be in the future. If you wish to play with the code yourself, the dependencies are in requirements.txt

### Demo
Since the project is setup for my spotify account, I recorded a short demo.
https://www.youtube.com/watch?v=XUqPH7kG1Hs